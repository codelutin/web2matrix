FROM golang:alpine AS builder

RUN mkdir /build
WORKDIR /build
COPY . .

RUN GOPROXY=https://proxy.golang.org CGO_ENABLED=0 GOOS=linux GOARCH=amd64 go build -a -ldflags="-w -s" -o /bin/web2matrix cmd/web2matrix/*.go

FROM gcr.io/distroless/static:nonroot

COPY --from=builder /bin/web2matrix /

# Expose port 8080 to the outside world
EXPOSE 8080

CMD ["/web2matrix"]
