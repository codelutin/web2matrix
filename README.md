Allow send matrix message throw simple http GET

All configuration is done via env variable:
- HOST_PORT (default ":8080") interface and port where server must listen
- MATRIX_SERVER (default: "https://matrix.cloud.codelutin.com") matrix server to use
- MATRIX_USER_* mapping between matrix user and token for user (ex: MATRIX_USER_bpoussin='MDAxYmxvY2F0a...')
- MATRIX_ROOM_* mapping between matrix room and room id (ex: MATRIX_ROOM_caverne='!FAIAZERTYUIOPQSDFG:codelutin.com')

Message is query parameter 'msg'

call pattern:

  curl http://localhost:8080/msg/<USER>/<ROOM>?msg=<MESSAGE>&status=<firing|resolved>&name=<TYPE>

call example:
```
  curl http://localhost:8080/msg/bpoussin/caverne?msg=Hello
```

How to test localy during development
```
  docker build -t registry.nuiton.org/codelutin/web2matrix .
  docker run -it -p8080:8080 --env-file env registry.nuiton.org/codelutin/web2matrix
```

Supported client
* simple http GET
* panto POST alert
* alertmanager POST alert
