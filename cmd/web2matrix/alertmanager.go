package main

import (
	"context"
	"encoding/json"
	"fmt"
	"github.com/prometheus/alertmanager/template"
	"net/http"

	"maunium.net/go/mautrix/event"
)

func getMatrixMessageForAlertmanager(alert template.Alert) event.MessageEventContent {
	return getMatrixMessage(alert.Status, alert.Labels["severity"], alert.Annotations["summary"]+"<br>"+alert.Annotations["description"])
}

func handleAlertmanagerRequest(w http.ResponseWriter, r *http.Request) {

	if r.Method != http.MethodPost {
		w.WriteHeader(http.StatusMethodNotAllowed)
		return
	}

	payload := template.Data{}
	if err := json.NewDecoder(r.Body).Decode(&payload); err != nil {
		w.WriteHeader(http.StatusBadRequest)
	}

	logger.Printf("Received valid hook from %v", r.RemoteAddr)

	matrixClient, targetRoomID, err := getMatrixClient(r)
	if err != nil {
		msg := fmt.Sprintf(">> Could not initialize Matrix client: %v", err)
		logger.Println(msg)
		http.Error(w, msg, 500)
		return
	}

	for _, alert := range payload.Alerts {
		msg := getMatrixMessageForAlertmanager(alert)
		logger.Printf("> %v", msg.Body)
		_, err := matrixClient.SendMessageEvent(context.TODO(), targetRoomID, event.EventMessage, msg)
		if err != nil {
			logger.Printf(">> Could not forward to Matrix: %v", err)
		}
	}

	w.WriteHeader(http.StatusOK)
}
