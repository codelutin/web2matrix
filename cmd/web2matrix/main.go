package main

// HOST_PORT (default ":8080") interface and port where server must listen
// MATRIX_SERVER (default: "matrix.cloud.codelutin.com") matrix server to use
// MATRIX_USER_* token for user *
// MATRIX_ROOM_* id for room *

import (
	"io"
	"log"
	"net/http"
	"os"
)

var logger *log.Logger

func main() {
	// Initialize logger.
	logger = log.New(os.Stdout, "", log.Flags())

	// Initialize HTTP server.
	http.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		io.WriteString(w, "OK\n")
	})
	http.Handle("/msg/", http.StripPrefix("/msg/", http.HandlerFunc(handleMsgRequest)))
	http.Handle("/alertmanager/", http.StripPrefix("/alertmanager/", http.HandlerFunc(handleAlertmanagerRequest)))
	http.Handle("/panto/", http.StripPrefix("/panto/", http.HandlerFunc(handlePantoRequest)))

	port := os.Getenv("HOST_PORT")
	if isEmpty(port) {
		port = ":8080"
	}

	logger.Printf("Listening for HTTP requests (webhooks) on %v", port)
	logger.Fatal(http.ListenAndServe(port, nil))
}
