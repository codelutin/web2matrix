package main

import (
	"context"
	"fmt"
	"html"
	"net/http"
	"os"
	"regexp"
	"strings"

	"maunium.net/go/mautrix"
	"maunium.net/go/mautrix/event"
	"maunium.net/go/mautrix/id"
)

func getMatrixMessage(status string, name string, msg string) event.MessageEventContent {
	var prefix string
	switch status {
	case "firing":
		prefix = "<strong><font color=\"#ff0000\">FIRING</font></strong> "
	case "resolved":
		prefix = "<strong><font color=\"#33cc33\">RESOLVED</font></strong> "
	default:
		prefix = fmt.Sprintf("<strong>%v</strong> ", status)
	}

	htmlText := prefix + name + " >> " + msg
	htmlRegex := regexp.MustCompile("<[^<]+?>")

	return event.MessageEventContent{
		Body:          html.UnescapeString(htmlRegex.ReplaceAllLiteralString(htmlText, "")),
		MsgType:       event.MsgText,
		Format:        event.FormatHTML,
		FormattedBody: htmlText,
	}
}

func getMatrixClient(r *http.Request) (*mautrix.Client, id.RoomID, error) {
	path := strings.TrimSuffix(r.URL.Path, "/") // trim a possible trailing slash
	pathComponents := strings.Split(path, "/")

	if len(pathComponents) != 2 {
		return nil, "", fmt.Errorf("Expected 2 path components, got %v: %v", len(path), path)
	}

	user := pathComponents[0]
	room := pathComponents[1]
	tokenKey := "MATRIX_USER_" + user
	targetRoomKey := "MATRIX_ROOM_" + room

	token, err := getNotNullEnv(tokenKey)
	if err != nil {
		return nil, "", fmt.Errorf("Token not found for user %v: %w", user, err)
	}

	targetRoom, err := getNotNullEnv(targetRoomKey)
	if err != nil {
		return nil, "", fmt.Errorf("ID not found for room %v: %w", room, err)
	}

	homeserver := getMatrixServer()

	logger.Printf("Connecting to Matrix Homserver %v as %v.", homeserver, user)

	userId := id.NewUserID(user, homeserver)
	// mautrix/id has not yet a NewRoomID method
	targetRoomID := id.RoomID(targetRoom)

	matrixClient, err := mautrix.NewClient(homeserver, userId, token)
	if err != nil {
		return nil, "", fmt.Errorf("Could not log in to Matrix Homeserver (%v): %w", homeserver, err)
	}

	joinedRooms, err := matrixClient.JoinedRooms(context.TODO())
	if err != nil {
		return nil, "", fmt.Errorf("Could not fetch Matrix rooms: %w", err)
	}

	alreadyJoinedTarget := false
	for _, roomID := range joinedRooms.JoinedRooms {
		if targetRoomID == roomID {
			alreadyJoinedTarget = true
		}
	}

	if alreadyJoinedTarget {
		logger.Printf("%v is already part of %v.", user, targetRoomID)
	} else {
		logger.Printf("Joining %v.", targetRoomID)
		_, err := matrixClient.JoinRoomByID(context.TODO(), targetRoomID)
		if err != nil {
			return nil, "", fmt.Errorf("Failed to join %v: %w", targetRoomID, err)
		}
	}

	return matrixClient, targetRoomID, nil
}

func getMatrixServer() string {
	result := os.Getenv("MATRIX_SERVER")
	if isEmpty(result) {
		return "https://matrix.cloud.codelutin.com"
	}

	return result
}

func getNotNullEnv(key string) (string, error) {
	value, ok := os.LookupEnv(key)
	if !ok {
		return "", fmt.Errorf("Missing env var " + key)
	}
	if isEmpty(value) {
		return "", fmt.Errorf("Empty env var " + key)
	}
	return value, nil
}
