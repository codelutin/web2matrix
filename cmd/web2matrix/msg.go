package main

import (
	"context"
	"fmt"
	"net/http"

	"maunium.net/go/mautrix/event"
)

func handleMsgRequest(w http.ResponseWriter, r *http.Request) {
	// Requêtes GET: fonctionnement original, gardé temporairement pour la rétrocompatibilité
	// Requêtes POST: semble plus logique, et permet d'envoyer des requêtes avec la version Busybox de wget
	if r.Method == http.MethodGet || r.Method == http.MethodPost {
		status := r.FormValue("status")
		name := r.FormValue("name")
		msg := r.FormValue("msg")

		if isEmpty(msg) {
			http.Error(w, "Missing msg parameter", 400)
			return
		}

		msgFormated := getMatrixMessage(status, name, msg)
		logger.Printf("Will send: %v", msgFormated.Body)
		matrixClient, targetRoomID, err := getMatrixClient(r)
		if err != nil {
			msg := fmt.Sprintf(">> Could not initialize Matrix client: %v", err)
			logger.Println(msg)
			http.Error(w, msg, 500)
			return
		}
		_, err = matrixClient.SendMessageEvent(context.TODO(), targetRoomID, event.EventMessage, msgFormated)
		if err != nil {
			msg := fmt.Sprintf(">> Could not forward to Matrix: %v", err)
			logger.Println(msg)
			http.Error(w, msg, 500)
			return
		}

	} else {
		http.Error(w, "Invalid request method.", 405)
		return
	}
}
