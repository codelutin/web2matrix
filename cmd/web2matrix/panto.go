package main

import (
	"context"
	"encoding/json"
	"fmt"
	"log"
	"net/http"

	"maunium.net/go/mautrix/event"
)

type pantoMsg struct {
	title    string // a title for the Alert
	message  string // why the Alert was triggered
	target   string // the display name of the Target that exhibits abnormal conditions
	agent    string // the display name of the Agent that ran the Probe on the Target
	probe    string // the name of the Probe that got abnormal results
	state    string // the current state, WARNING or CRITICAL
	reason   string // the user-supplied reason in the check(s) that weren't validated
	critical bool   // true if state is CRITICAL
	warning  bool   // true if state is WARNING
}

func handlePantoRequest(w http.ResponseWriter, r *http.Request) {
	if r.Method == http.MethodPost {

		decoder := json.NewDecoder(r.Body)
		var t pantoMsg
		err := decoder.Decode(&t)
		if err != nil {
			log.Println(err)
			http.Error(w, fmt.Sprintf("%s", err), 400)
			return
		}

		msg := fmt.Sprintf("(%s)\n\n%s\n%s\nbecause\n%s\n\n%s", t.target, t.probe, t.message, t.reason, t.agent)

		matrixClient, targetRoomID, err := getMatrixClient(r)
		if err != nil {
			msg := fmt.Sprintf(">> Could not initialize Matrix client: %v", err)
			logger.Println(msg)
			http.Error(w, msg, 500)
			return
		}

		msgFormated := getMatrixMessage(t.state, t.title, msg)
		logger.Printf("> %v", msgFormated.Body)
		_, err = matrixClient.SendMessageEvent(context.TODO(), targetRoomID, event.EventMessage, msgFormated)
		if err != nil {
			logger.Printf(">> Could not forward to Matrix: %v", err)
			http.Error(w, "Could not forward to Matrix.", 500)
			return
		}

	} else {
		http.Error(w, "Invalid request method.", 405)
		return
	}
}
