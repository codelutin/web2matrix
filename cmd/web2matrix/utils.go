package main

import (
	"strings"
)

// No built-in util in strings ?
func isEmpty(s string) bool {
	return len(strings.TrimSpace(s)) == 0
}
